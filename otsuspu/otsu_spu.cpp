#include <stdio.h>
#include <stdint.h>
#include <spu_mfcio.h>
#include <string>
#include <cmath>

// only really use 5, the sixth is for alignment (bus error otherwise)
// can be used for extra data 
struct VerticalChunkAddressStructure
{
	unsigned long long int chunkData[6];
};

/*
	Function that performs Otsu Thresholding on the Image Data it essentially makes a histogram of the
	data and then calculates where the threshold in relation to the total data should be. Normally
	we would do it with the whole of the image data but in this case we can do it with the chunked data
	with fairly little degredation in the outcome (even some improvements actually). Of note is that it 
	modifies the image data passed in and doesn't return it, so be aware of that.

	@uint8_t imageData - the image data in a 2d array that we wish to work on 
	@int xSize - number of elements the 2D array has on the X
	@int ySize - number of elements the 2D array has on the Y (Y is the first index in this case)
*/
void otsuThresholding(uint8_t imageData[][128], int xSize, int ySize)
{
	// Otsu method
	float threshold = 0;
	float varMax = 0;
	float sumBackground = 0;
	float sum = 0;
	int weightBackground = 0;
	int total;
	int weightForeground;
	float meanBackground;
	float meanForeground;
	float between;
	
	// Histogram of all the values we can attain from a greyscale image 1-256 (0-255 really)
	int histogram[256] = { 0 };

	int i, j;
	// create our histogram
	for (i = 0; i < ySize; ++i)
		for (j = 0; j < xSize; ++j)
			histogram[imageData[i][j]] ++;

	for (i = 1; i < 256; ++i)
		sum += i * histogram[i];

	// Calculate the Otsu Thresholding, essentially working out the median value between
	// the background and foreground so we can cut the background out
	for (i = 0; i < 256; ++i)
	{

		weightBackground += histogram[i];
		if (weightBackground == 0) continue;

		total = xSize * ySize;

		weightForeground = total - weightBackground;

		if (weightForeground == 0) break;

		sumBackground += (float)(i * histogram[i]);

		meanBackground = sumBackground / weightBackground;
		meanForeground = (sum - sumBackground) / weightForeground;
		between = (float)weightBackground * (float)weightForeground * pow(meanBackground - meanForeground, 2);

		if (between > varMax)
		{
			threshold = i;
			varMax = between;
		}

	}

	// Otsu thresholding check
	for (i = 0; i < ySize; ++i)
		for (j = 0; j < xSize; ++j)
		{
			if (imageData[i][j] < threshold)
				imageData[i][j] = 0;
		}
}


// function that pulls the chunk data from the DMA bus
// @uint8_t** appendData - pointer to an 1D array that we append the chunk to
// @int startRow - the integer place we start to append the data from, continues on for 96 more rows
// 				   as the chunk data should be received in 96 * 128 packets (well the memory addresses)
// 				   and altogether the appendData will eventually reach [480][128] one downward slice of the image
//				   out of 5
// @int chunkNumber - index value of the chunk we wish to access within the address structure
// @VerticalChunkAddressStructure dataAddresses - structure containing the sobel memory addresses, should already
// 										    be filled using mfc_get
void getChunkData(uint8_t appendData[][128], int startRow, int chunkNumber, VerticalChunkAddressStructure dataAddresses)
{
	uint8_t chunks[96][128];
	// works to bring in 2D array data
	mfc_get(&chunks, dataAddresses.chunkData[chunkNumber], sizeof(chunks), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
	
	int chunkI = 0;
	for(int i = startRow; i < startRow + 96; ++i, ++chunkI)
		memmove(&appendData[i], &chunks[chunkI], sizeof(chunks[chunkI]));
}

// function that puts the chunk data back onto the DMA bus/shared memory
// @uint8_t** appendData - pointer to an 1D array that we append the chunk to
// @int startRow - the integer place we start to append the data from, continues on for 96 more rows
// 				   as the chunk data should be received in 96 * 128 packets (well the memory addresses)
// 				   and altogether the appendData will eventually reach [480][128] one downward slice of the image
//				   out of 5
// @int chunkNumber - index value of the chunk we wish to access within the address structure
// @VerticalChunkAddressStructure dataAddresses - structure containing the sobel memory addresses, should already
// 										    be filled using mfc_get
void putChunkData(uint8_t appendData[][128], int startRow, int chunkNumber, VerticalChunkAddressStructure dataAddresses)
{
	uint8_t chunks[96][128];
	
	int chunkI = 0;
	for(int i = startRow; i < startRow + 96; ++i, ++chunkI)
		memmove(&chunks[chunkI], &appendData[i], sizeof(appendData[i]));
		
	mfc_put(chunks, dataAddresses.chunkData[chunkNumber], sizeof(chunks), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
}

int main(unsigned long long spe_id, unsigned long long argp,
 unsigned long long envp)
{
	// 60kb's of data so we can split the image into 6x
	VerticalChunkAddressStructure dataAddresses __attribute__((aligned(16)));
	
	// should be uint8_t && 96 * 128 (maybe more would need to recalculate)
	// get the memory addresses of the data
	mfc_get(&dataAddresses, argp, sizeof(dataAddresses), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
	
	uint8_t combinedData[480][128];
	
	getChunkData(combinedData, 0, 0, dataAddresses);
	getChunkData(combinedData, 96, 1, dataAddresses);
	getChunkData(combinedData, 192, 2, dataAddresses);
	getChunkData(combinedData, 288, 3, dataAddresses);
	getChunkData(combinedData, 384, 4, dataAddresses);
	
	otsuThresholding(combinedData, 128, 480);
	
	putChunkData(combinedData, 0, 0, dataAddresses);
	putChunkData(combinedData, 96, 1, dataAddresses);
	putChunkData(combinedData, 192, 2, dataAddresses);
	putChunkData(combinedData, 288, 3, dataAddresses);
	putChunkData(combinedData, 384, 4, dataAddresses);	

	return 0;
}