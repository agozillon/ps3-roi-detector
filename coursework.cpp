#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <libspe2.h>
#include <pthread.h>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include <ctime>
#include <iostream>
#include <sstream>
#include <ppu_intrinsics.h>
#include "ppm.h"

extern spe_program_handle_t sobel_spu;
extern spe_program_handle_t otsu_spu;
extern spe_program_handle_t sobelandotsu_spu;
extern spe_program_handle_t sumfunction_spu;
extern spe_program_handle_t sobelandotsuandsum_spu;

const int TICKS_PER_SEC = 79800000;

// only really use 5, the sixth is for alignment (bus error otherwise)
// can be used for extra data 
struct VerticalChunkAddressStructure
{
	unsigned long long int chunkData[6];
};

// Arguement list that you can pass to a ppu thread as the void* arg 
// can then retrieve whatever is in the argument, such as the spe_context_ptr_t to the spe
struct ThreadArguementList
{
	spe_context_ptr_t speCTX;         // the SPE context
	void* memoryAddressOfAddressData; // your structure
};

// Defines a box structure that uses the top left corner and bottom right corner to define it's size 
struct Box
{
	int topLx;
	int topLy;
	int botRx;
	int botRy;
	
	// returns true if a box is inside another box  
	bool isInside(Box collider){
	
		if( collider.topLx < botRx && collider.topLx > topLx
			 && collider.botRy < botRy && collider.botRy > topLy )
			return true;
			
		if( collider.topLx < botRx && collider.topLx > topLx
			&& collider.topLy < botRy && collider.topLy > topLy)
			return true;
						
		if ( collider.botRx < botRx && collider.botRx > topLx
			&& collider.topLy < botRy && collider.topLy > topLy)
			return true;
			
		if ( collider.botRx < botRx && collider.botRx > topLx
			&& collider.botRy < botRy && collider.botRy > topLy)
			return true;
			
		return false;
		
	}
};
					
// UTILITY FUNCTIONS LOADERS, WRITERS AND THREAD CREATORS

// creates a posix thread using an SPE context
// @void* arg - the spe_context_ptr_t as a void* that you wish to run on the thread
void* ppu_pthread_function(void* arg)
{
	ThreadArguementList arguements;
	unsigned int entry = SPE_DEFAULT_ENTRY;
	arguements = *((ThreadArguementList *)arg); // cast void * to spe_context_ptr_t
	
	if(spe_context_run(arguements.speCTX, &entry, 0, arguements.memoryAddressOfAddressData, NULL, NULL) < 0)
	{
		perror("failed running context");
		exit(1);
	}
	
	pthread_exit(NULL); 
}

// function that loads in CSV data
std::vector< std::vector<uint8_t> > loadCSV(char* fileName)
{
	std::fstream frame(fileName);

	// all data when loaded into the test vector is accessed as [Y] for the first vector and [X] as the second
	// instead of the more intuitive [X][Y]
	std::vector< std::vector<uint8_t> > fileVector;

	// push our first vector 
	fileVector.push_back(std::vector<uint8_t>());

	//if file is open
	if (frame.is_open())
	{
		int j = 0;
		char output;
		std::string inputValue;
		// while not end of file do this
		while (!frame.eof() && frame.good())
		{
			output = frame.get(); // get character

			// if output from file is a digit add it to the string
			if (isdigit(output))
			{
				inputValue += output;
			}

			// if output from file is a newline. space, comma, terminating character OR the end of file
			// then the current number is finished so convert it to an integer, set the string to blank
			// and create a new column
			if (output == '\n' || output == ' ' || output == ',' || output == '\0' || frame.eof())
			{

				if (inputValue.size() > 0) // if the string actually has anything in it
					fileVector[j].push_back(std::atoi((inputValue.c_str())));

				inputValue.clear();

				if (output == '\n')
				{
					j++;
					std::vector<uint8_t> newCollumn;
					fileVector.push_back(std::vector<uint8_t>());
				}
			}
		}
	}

	frame.close();

	return fileVector;
}

std::vector< std::vector<uint8_t> > sobelEdgeDetection(std::vector< std::vector<uint8_t> > imageData, bool useApproximation)
{
	// first standard filter, flipped so that the first array is the x and second the y 
	// when writing it the Y is top down so 0 is the top 2 is the bottom, 0, 0 would be the top leaf
	// also [Y][X] when accessing array, (the loaded in data is also like this, so kept the array this way instead
	// of re-arranging it.)
	int sobelXKernel[3][3] = { { 1, 5, 1 },
	{ 0, 0, 0 },
	{ -1, -5, -1 } };


	// same filter rotated 90 degrees, flipped so that the first array is the x and second the y 	
	// when writing it the Y is top down so 0 is the top 2 is the bottom, 0, 0 would be the top left
	// also [Y][X] when accessing array
	int sobelYKernel[3][3] = { { -1, 0, 1 },
	{ -5, 0, 5 },
	{ -1, 0, 1 } };

	// create vectors and resize them for the data we're about to create for the X kernel gradients 
	// and Y Kernel gradients
	std::vector< std::vector<int> > kernelXValues;
	kernelXValues.resize(imageData.size());
	for (unsigned int i = 0; i < kernelXValues.size(); i++)
		kernelXValues[i].resize(imageData[i].size());

	std::vector< std::vector<int> > kernelYValues;
	kernelYValues.resize(imageData.size());
	for (unsigned int i = 0; i < kernelYValues.size(); i++)
		kernelYValues[i].resize(imageData[i].size());

	bool upFlag = false, downFlag = false, leftFlag = false, rightFlag = false;
	int averageAtXPoint, averageAtYPoint, topLeft, centerLeft, bottomLeft, topCenter, bottomCenter, topRight, centerRight, bottomRight;
	for (unsigned int i = 0; i < imageData.size(); i++)
	{
		for (unsigned int j = 0; j < imageData[i].size(); j++)
		{
			//reset values
			upFlag = false, downFlag = false, rightFlag = false, leftFlag = false;
			averageAtXPoint = 0; averageAtYPoint = 0;

			// checking if the grid overextends
			// extends too far up
			if ((int)i - 1 < 0)
				upFlag = true;

			// extends too far down
			if (i + 1 >= imageData.size())
				downFlag = true;

			// extends too far right
			if (j + 1 >= imageData[i].size())
				rightFlag = true;

			// extends too far left
			if ((int)j - 1 < 0)
				leftFlag = true;
			
			// based on what edge it goes off of, we copy the data from one of the existing neighbour cells
			// into it and use that instead
			if (upFlag)
				topCenter = imageData[i][j];
			else
				topCenter = imageData[i - 1][j];

			if (leftFlag)
				centerLeft = imageData[i][j];
			else
				centerLeft = imageData[i][j - 1];

			if (rightFlag)
				centerRight = imageData[i][j];
			else
				centerRight = imageData[i][j + 1];

			if (downFlag)
				bottomCenter = imageData[i][j];
			else
				bottomCenter = imageData[i + 1][j];

			if (leftFlag || upFlag)
				topLeft = imageData[i][j];
			else
				topLeft = imageData[i - 1][j - 1];

			if (rightFlag || upFlag)
				topRight = imageData[i][j];
			else
				topRight = imageData[i - 1][j + 1];

			if (downFlag || rightFlag)
				bottomRight = imageData[i][j];
			else
				bottomRight = imageData[i + 1][j + 1];

			if (leftFlag || downFlag)
				bottomLeft = imageData[i][j];
			else
				bottomLeft = imageData[i + 1][j - 1];

			// Calculate X Points
			// top row, remember both Sobel and Data are accessed as [Y][X] not [X][Y]
			averageAtXPoint += topLeft * sobelXKernel[0][0]; // left
			averageAtXPoint += topCenter * sobelXKernel[0][1];   // center
			averageAtXPoint += topRight * sobelXKernel[0][2]; // right

			// middle row
			averageAtXPoint += centerLeft * sobelXKernel[1][0]; // center left * centered left value	
			averageAtXPoint += imageData[i][j] * sobelXKernel[1][1]; // Center filter * centered value
			averageAtXPoint += centerRight * sobelXKernel[1][2]; // center right * center right value

			// bottom row
			averageAtXPoint += bottomLeft * sobelXKernel[2][0]; // left
			averageAtXPoint += bottomCenter * sobelXKernel[2][1]; // center
			averageAtXPoint += bottomRight * sobelXKernel[2][2]; // right

			// Calculate Y Points
			// top row, remember both Sobel and Data are accessed as [Y][X] not [X][Y]
			averageAtYPoint += topLeft * sobelYKernel[0][0]; // left
			averageAtYPoint += topCenter * sobelYKernel[0][1];   // center
			averageAtYPoint += topRight * sobelYKernel[0][2]; // right

			// middle row
			averageAtYPoint += centerLeft * sobelYKernel[1][0]; // center left * centered left value	
			averageAtYPoint += imageData[i][j] * sobelYKernel[1][1]; // Center filter * centered value
			averageAtYPoint += centerRight * sobelYKernel[1][2]; // center right * center right value

			// bottom row
			averageAtYPoint += bottomLeft * sobelYKernel[2][0]; // left
			averageAtYPoint += bottomCenter * sobelYKernel[2][1]; // center
			averageAtYPoint += bottomRight * sobelYKernel[2][2]; // right

			kernelYValues[i][j] = averageAtYPoint / 9;
			kernelXValues[i][j] = averageAtXPoint / 9; // averaged area at center point
		}
	}

	if (useApproximation == true)
	{
		// approximate magnitude as described in paper
		for (unsigned int i = 0; i < imageData.size(); i++)
			for (unsigned int j = 0; j < imageData[i].size(); j++)
				imageData[i][j] = kernelXValues[i][j] + kernelYValues[i][j];
	}
	else
	{
		// exact magnitude as described in paper
		for (unsigned int i = 0; i < imageData.size(); i++)
			for (unsigned int j = 0; j < imageData[i].size(); j++)
				imageData[i][j] = (uint8_t)sqrt(pow(kernelXValues[i][j], 2) + pow(kernelYValues[i][j], 2));
	}

	return imageData;
}

std::vector< std::vector<uint8_t> > otsuThresholding(std::vector< std::vector<uint8_t> > imageData)
{
	// Otsu method
	float threshold = 0;
	float varMax = 0;
	float sumBackground = 0;
	float sum = 0;

	int weightBackground = 0;

	// Histogram of all the values we can attain from a greyscale image 1-256 (0-255 really)
	int histogram[256] = { 0 };

	// create our histogram
	for (unsigned int i = 0; i < imageData.size(); i++)
		for (unsigned int j = 0; j < imageData[i].size(); j++)
			histogram[imageData[i][j]] ++;

	for (int i = 1; i < 256; i++)
		sum += i * histogram[i];

	for (int i = 0; i < 256; i++)
	{

		weightBackground += histogram[i];
		if (weightBackground == 0) continue;

		int total = imageData.size() * imageData[0].size();

		int weightForeground = total - weightBackground;

		if (weightForeground == 0) break;

		sumBackground += (float)(i * histogram[i]);

		float meanBackground = sumBackground / weightBackground;
		float meanForeground = (sum - sumBackground) / weightForeground;
		float between = (float)weightBackground * (float)weightForeground * pow(meanBackground - meanForeground, 2);

		if (between > varMax)
		{
			threshold = i;
			varMax = between;
		}

	}

	// Otsu thresholding check
	for (unsigned int i = 0; i < imageData.size(); i++)
		for (unsigned int j = 0; j < imageData[i].size(); j++)
		{
			if (imageData[i][j] < threshold)
				imageData[i][j] = 0;
		}

	return imageData;
}

std::vector< std::vector<int> > basicRegionSumming(std::vector< std::vector<uint8_t> > imageData, unsigned int sizeOfRegionInX, unsigned int sizeOfRegionInY)
{
// The Table all our results will end up in and then be returned
	std::vector< std::vector<int> > sumAreaTable;
	sumAreaTable.resize(imageData.size() / sizeOfRegionInY); // calculate table size in Y
	for (unsigned int i = 0; i < sumAreaTable.size(); i++)
		sumAreaTable[i].resize(imageData[i].size() / sizeOfRegionInX);
	
	// the table we sum the results into to get each integral value for each region
	std::vector< std::vector<int> >sumAreaRegion;
	sumAreaRegion.resize(sizeOfRegionInY);
	for (unsigned int i = 0; i < sumAreaRegion.size(); i++)
		sumAreaRegion[i].resize(sizeOfRegionInX);


	for (unsigned int j2 = 0; j2 < sumAreaTable.size(); j2++)
	{
		for (unsigned int i2 = 0; i2 < sumAreaTable[j2].size(); i2++)
		{
			unsigned int regionXStart = i2 * sizeOfRegionInX;
			unsigned int regionYStart = j2 * sizeOfRegionInY;

			for (unsigned int i3 = 0; i3 < sizeOfRegionInY; i3++)
				for (unsigned int j3 = 0; j3 < sizeOfRegionInX; j3++)
					sumAreaRegion[i3][j3] = imageData[regionYStart + i3][regionXStart + j3];

			// loop through all x and y, current value += previous X Value + previous Y Value -  
			for (unsigned int i = 0; i < sizeOfRegionInY; i++)
				for (unsigned int j = 0; j < sizeOfRegionInX; j++)
					sumAreaTable[j2][i2] += sumAreaRegion[i][j];

			sumAreaRegion.empty();
		}
	}

	return sumAreaTable;
}

// Integral Image/Summation Function returns the image data summed up in regions
std::vector< std::vector<int> > integralImage(std::vector< std::vector<uint8_t> > imageData, unsigned int sizeOfRegionInX, unsigned int sizeOfRegionInY)
{
	// The Table all our results will end up in and then be returned
	std::vector< std::vector<int> > sumAreaTable;
	sumAreaTable.resize(imageData.size() / sizeOfRegionInY); // calculate table size in Y
	for (unsigned int i = 0; i < sumAreaTable.size(); i++)
		sumAreaTable[i].resize(imageData[i].size() / sizeOfRegionInX);
	
	// the table we sum the results into to get each integral value for each region
	std::vector< std::vector<int> >sumAreaRegion;
	sumAreaRegion.resize(sizeOfRegionInY);
	for (unsigned int i = 0; i < sumAreaRegion.size(); i++)
		sumAreaRegion[i].resize(sizeOfRegionInX);


	for (unsigned int j2 = 0; j2 < sumAreaTable.size(); j2++)
	{
		for (unsigned int i2 = 0; i2 < sumAreaTable[j2].size(); i2++)
		{
			unsigned int regionXStart = i2 * sizeOfRegionInX;
			unsigned int regionYStart = j2 * sizeOfRegionInY;

			for (unsigned int i3 = 0; i3 < sizeOfRegionInY; i3++)
				for (unsigned int j3 = 0; j3 < sizeOfRegionInX; j3++)
					sumAreaRegion[i3][j3] = imageData[regionYStart + i3][regionXStart + j3];

			// loop through x add previous x value onto current x value
			for (unsigned int i = 1; i < sizeOfRegionInX; i++)
			{
				sumAreaRegion[0][i] += sumAreaRegion[0][i - 1];
			}

			// loop through y add previous y value onto current y value
			for (unsigned int j = 1; j < sizeOfRegionInY; j++)
			{
				sumAreaRegion[j][0] += sumAreaRegion[j - 1][0];
			}

			// loop through all x and y, current value += previous X Value + previous Y Value -  
			for (unsigned int i = 1; i < sizeOfRegionInY; i++)
				for (unsigned int j = 1; j < sizeOfRegionInX; j++)
					sumAreaRegion[i][j] += sumAreaRegion[i][j - 1] + sumAreaRegion[i - 1][j] - sumAreaRegion[i - 1][j - 1];

			sumAreaTable[j2][i2] = sumAreaRegion[sumAreaRegion.size() - 1][sumAreaRegion[0].size() - 1];

			sumAreaRegion.empty();
		}
	}

	return sumAreaTable;
}

std::vector< std::vector<uint8_t> > applyROIToImage(std::vector< std::vector<uint8_t> > imageData, std::vector< std::vector<int> > integralImageData, int sizeOfRegionInX, int sizeOfRegionInY, int integralImageThreshold)
{

	std::vector< std::vector< Box > > cornerPoints = std::vector< std::vector< Box > >(integralImageData.size(), std::vector< Box >(integralImageData[0].size())); ;
	std::vector< std::vector< std::pair<unsigned int, unsigned int> > > indexs = std::vector< std::vector< std::pair<unsigned int, unsigned int> > >(integralImageData.size(), std::vector< std::pair<unsigned int, unsigned int> >(integralImageData[0].size()));
	unsigned int i, j; 

	// Basically if a region has enough data in it to be considered an object then give it a size 
	// the indexs also get created here (as it's the same size as the integralImageData/cornerPoints
	// so can optimize it by sticking it in the one loop).  All indexs are, are representations of 
	// what areas a box owns. i.e. a box that starts at 0, 0 can later own boxes 0, 0, to 5, 5 depending on box
	// conjoining.
	for (i = 0; i < integralImageData.size(); ++i)
	{
		for (j = 0; j < integralImageData[i].size(); ++j)
		{
			if (integralImageData[i][j] >  integralImageThreshold)
			{
				cornerPoints[i][j].topLx = j * sizeOfRegionInX;
				cornerPoints[i][j].topLy = i * sizeOfRegionInY;
				cornerPoints[i][j].botRx = j * sizeOfRegionInX + sizeOfRegionInX - 1;
				cornerPoints[i][j].botRy = i * sizeOfRegionInY + sizeOfRegionInY - 1;
			}
			
			indexs[i][j].first = i;
			indexs[i][j].second = j;
		}
	}

	int currentI = 0, currentJ = 0;
	bool tag = false;
	
	// Removes adjacent boxes by creating one larger box for the X axis
	for (i = 0; i < cornerPoints.size(); ++i)
	{
		for (j = 0; j < cornerPoints[i].size() - 1; ++j)
		{
			if (tag == false)
			{
				currentI = i;
				currentJ = j;
			}

			if (cornerPoints[currentI][currentJ].botRx + sizeOfRegionInX == cornerPoints[i][j + 1].botRx)
			{
				cornerPoints[currentI][currentJ].botRx = cornerPoints[i][j + 1].botRx;

				cornerPoints[i][j + 1].botRx = cornerPoints[i][j + 1].botRy = cornerPoints[i][j + 1].topLx = cornerPoints[i][j + 1].topLy = 0;
				
				indexs[i][j + 1].first = currentI;
				indexs[i][j + 1].second = currentJ;
			
				tag = true;
			}
			else
				tag = false;
		}
	}

	tag = false;
	unsigned int startXIndex, endXIndex, startYIndex, endYIndex, i2, j2;
	
	// Removes adjacent boxes by creating one larger box for the Y Axis
	for (j = 0; j < cornerPoints[0].size(); ++j)
	{
		for (i = 0; i < cornerPoints.size() - 1; ++i)
		{
			if (tag == false)
			{
				currentI = i;
				currentJ = j;
			}

			if (cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].botRy + 1
				== cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].topLy)
			{
				cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].botRy = cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].botRy;

				if (cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].botRx < cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].botRx)
					cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].botRx = cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].botRx;
			
				if (cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].topLx > cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].topLx)
					cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].topLx = cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].topLx;

				cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].botRx = cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].botRy = 0;
				cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].topLx = cornerPoints[indexs[i + 1][j].first][indexs[i + 1][j].second].topLy = 0;
			
				// for every Index inside the box set it to the current index (as if it was the same index i.e one single box)
				startXIndex = (cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].topLx / sizeOfRegionInX);
				endXIndex = ((cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].botRx + 1) / sizeOfRegionInX);  
				startYIndex = (cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].topLy / sizeOfRegionInY);
				endYIndex = ((cornerPoints[indexs[currentI][currentJ].first][indexs[currentI][currentJ].second].botRy + 1) / sizeOfRegionInY); 
			
				for(i2 = startYIndex; i2 < endYIndex; ++i2)
					for(j2 = startXIndex; j2 < endXIndex; ++j2)
					{
						indexs[i2][j2].first = indexs[currentI][currentJ].first;
						indexs[i2][j2].second = indexs[currentI][currentJ].second;
					}	

				tag = true;
			}
			else
				tag = false;

		}
	}
	
	
	// Merges internal boxes
	for (j = 0; j < cornerPoints[0].size(); ++j)
	{
		for (i = 0; i < cornerPoints.size(); ++i)
		{
			if (cornerPoints[i][j].botRx > 0 || cornerPoints[i][j].botRy > 0
				|| cornerPoints[i][j].topLx > 0 || cornerPoints[i][j].topLy > 0)
			{
				for (j2 = 0; j2 < cornerPoints[0].size(); ++j2)
				{
					for (i2 = 0; i2 < cornerPoints.size(); ++i2)
					{
						// first check in brackets just checks it IS a box second is just a collision check 
						if ((cornerPoints[i2][j2].botRx > 0 || cornerPoints[i2][j2].botRy > 0
							|| cornerPoints[i2][j2].topLx > 0 || cornerPoints[i2][j2].topLy > 0) 
							&& cornerPoints[i][j].isInside(cornerPoints[i2][j2]))
						{
								if (cornerPoints[i2][j2].topLx < cornerPoints[i][j].topLx)
									cornerPoints[i][j].topLx = cornerPoints[i2][j2].topLx;

								if (cornerPoints[i2][j2].topLy < cornerPoints[i][j].topLy)
									cornerPoints[i][j].topLy = cornerPoints[i2][j2].topLy;

								if (cornerPoints[i2][j2].botRx > cornerPoints[i][j].botRx)
									cornerPoints[i][j].botRx = cornerPoints[i2][j2].botRx;

								if (cornerPoints[i2][j2].botRy > cornerPoints[i][j].botRy)
									cornerPoints[i][j].botRy = cornerPoints[i2][j2].botRy;

					
								cornerPoints[i2][j2].topLx = cornerPoints[i2][j2].topLy = cornerPoints[i2][j2].botRx = cornerPoints[i2][j2].botRy = 0;
						}
					}
				}
			}
		}
	}


	int lineSize, k;
	for (i = 0; i < cornerPoints.size(); ++i)
	{
		for (j = 0; j < cornerPoints[i].size(); ++j)
		{
			if (cornerPoints[i][j].botRx != 0 && cornerPoints[i][j].botRy != 0)
			{
				// top horizontal line
				lineSize = cornerPoints[i][j].botRx - cornerPoints[i][j].topLx;
				for (k = 0; k < lineSize; ++k)
					imageData[cornerPoints[i][j].topLy][cornerPoints[i][j].topLx + k] = 255;

				//bottom horizontal line
				for (k = 0; k < lineSize; ++k)
					imageData[cornerPoints[i][j].botRy][cornerPoints[i][j].topLx + k] = 255;

				// left vertical line
				lineSize = cornerPoints[i][j].botRy - cornerPoints[i][j].topLy;
				for (k = 0; k < lineSize; ++k)
					imageData[cornerPoints[i][j].botRy - k][cornerPoints[i][j].topLx] = 255;

				// right vertical line
				for (k = 0; k < lineSize; ++k)
					imageData[cornerPoints[i][j].botRy - k][cornerPoints[i][j].botRx] = 255;

			}
		}
	}
	
	return imageData;
}

// Function that chunks the sobel data and sets up the sobel address structure for sending
// to the SPU. It partitions it by width doing vertical slices of the image. around 480 pixels down(bytes)
// and 128 pixels across(128 bytes) total-ing out to working with 60kbs per SPU. That we retrieve via 5 chunks(chunkData).
// @vector<vector<uint8_t>> imageData - the image data as a whole
// @uint8_t [][][128] chunkData - a 3D array of uint8_t in this case its [5 chunks] of [96](Y axis) pixels by [128](X axis) pixels 
// @VerticalChunkAddressStructure dataAddresses - the address structure containing all the data pointers we intend to send to the 
//										   SPU 
// @int startWidth - this is an integer representing what width point in the image we start from and we then chunk fro this
// 					 point to 128 pixels to the right
void verticallyChunkData(std::vector< std::vector<uint8_t> > imageData, uint8_t chunkData[][96][128], VerticalChunkAddressStructure& dataAddresses, int startWidth)
{
	// chunk the data into arrays to send in 12kb chunks, less than the 16kb DMA transfers
	// essentially sends all of the Y from 0 - 480 and from 0-128 on the x. So we're sending
	// the data in 5 slices to each of the SPU's to work on
	int i = 0, chunkI = 0, chunkJ = 0;
	
	for(; i < 480; ++i, ++chunkJ)
	{		
		if(i == 96)
		{	
			chunkJ = 0;
			chunkI++;
		}		
		
		memmove(&chunkData[chunkI][chunkJ][0], &imageData[i][0] + startWidth, 128);
	}
	
	dataAddresses.chunkData[0] = 0 + (unsigned)chunkData[0];
	dataAddresses.chunkData[1] = 0 + (unsigned)chunkData[1];
	dataAddresses.chunkData[2] = 0 + (unsigned)chunkData[2];
	dataAddresses.chunkData[3] = 0 + (unsigned)chunkData[3];
	dataAddresses.chunkData[4] = 0 + (unsigned)chunkData[4];
}

// runVerticalChunkSPE essentially runs the passed in SPE program 5x(sending them to open SPU's as they're available
// so potential for up to 5 SPU's running parallel) using Vertical Chunks of data split into 5 squares of 96 Y Pixels x 128 X Pixels
// maxing out to 480 Y Pixels and 128 X pixels per SPE.
// @std::vector< std::vector<uint8_t> > imageData - param name explains it all, essentially your image data, it will be edited
//													 in the function from the returning data
// @spe_program_handle_t speHandle - handle for the SPE program you wish to run, has to use Vertical Chunk Data
// 									 at the moment, hasn't been generalized to use anything else (Sobel, Otsu and otsuandsobel 
//								     spe's fit into this category).
void runVerticalChunkSPE(spe_program_handle_t speHandle, std::vector< std::vector<uint8_t> > &imageData)
{
	// 5 sets of data for each SPU, 5 Chunks per SPU of 96 Y pixels x 128 X pixels 
	uint8_t chunkData[5][5][96][128] __attribute__((aligned(8)));
	
	// 5 data addresses for each SPE
	VerticalChunkAddressStructure dataAddresses[5] __attribute__((aligned(16)));
	
	// calculate number of SPUs available to create contexts for
	int iSPUsAvailable = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, -1);
	int iThreads = 5;

	// create a vector of contexts
	std::vector<spe_context_ptr_t> vecSPEContexts;
	std::vector<pthread_t> vecThreads;

	vecSPEContexts.resize(iThreads);
	vecThreads.resize(iThreads);
	ThreadArguementList list __attribute__((aligned(16)));
	unsigned int i;
	// create our SPU programs and run them using POSIX threads
	for(i = 0; i < vecThreads.size(); ++i)
	{
		if((vecSPEContexts[i] = spe_context_create(0, NULL)) == NULL)
		{
			perror("Failed creating SPE context");
			exit(1);
		}

		// load the program handle into the context
		if(spe_program_load(vecSPEContexts[i], &speHandle))
		{
			perror("Failed loading SPE program");
			exit(1);
		}
		
		// get our chunks for this SPU, 0 * 128 = 0, 1 * 128 = 128 etc.
		verticallyChunkData(imageData, chunkData[i], dataAddresses[i], i * 128);

		list.speCTX = vecSPEContexts[i];
		list.memoryAddressOfAddressData = &dataAddresses[i];

		if(pthread_create(&vecThreads[i], NULL, &ppu_pthread_function, &list))
		{	
			perror("Failed executing SPE Program");
			exit(1);
		}
	}
	
	// Check all our threads have completed and that it's safe to continue on
	for(i = 0; i < vecThreads.size(); ++i)
	{
		if(pthread_join(vecThreads[i], NULL))
		{
			perror("Failed pthread_join");
		}
		
		// Destroy Context 
		if(spe_context_destroy(vecSPEContexts[i]) != 0)
		{
			perror("Failed Destroying Context");
			exit(1);
		}
	}
	
	int chunkJ = 0;
	int chunkK = 0;
	
	for(i = 0; i < 480; ++i, ++chunkK)
	{
		if(i == 96)
		{
			chunkK = 0;
			chunkJ++;
		}
		
		memmove(&imageData[i][0], &chunkData[0][chunkJ][chunkK], sizeof(chunkData[0][chunkJ][chunkK]));	
		memmove(&imageData[i][0] + 128, &chunkData[1][chunkJ][chunkK], sizeof(chunkData[1][chunkJ][chunkK]));	
		memmove(&imageData[i][0] + 256, &chunkData[2][chunkJ][chunkK], sizeof(chunkData[2][chunkJ][chunkK]));	
		memmove(&imageData[i][0] + 384, &chunkData[3][chunkJ][chunkK], sizeof(chunkData[3][chunkJ][chunkK]));	
		memmove(&imageData[i][0] + 512, &chunkData[4][chunkJ][chunkK], sizeof(chunkData[4][chunkJ][chunkK]));
	}
}

// runSummedAreaTableSPE essentially runs the passed in SPE program 5x(sending them to open SPU's as they're available
// so potential for up to 5 SPU's running parallel) using Vertical Chunks of data split into 5 squares of 96 Y Pixels x 128 X Pixels
// maxing out to 480 Y Pixels and 128 X pixels per SPE.
// @std::vector< std::vector<uint8_t> > imageData - param name explains it all, essentially your image data, it will be edited
//													 in the function from the returning data
// @spe_program_handle_t speHandle - handle for the SPE program you wish to run, has to use Vertical Chunk Data
// 									 at the moment, hasn't been generalized to use anything else (Sobel, Otsu and otsuandsobel 
//								     spe's fit into this category).
void runSummedAreaTableSPE(spe_program_handle_t speHandle, std::vector< std::vector<uint8_t> > &imageData, std::vector< std::vector<int> > &summedImage)
{
	// 5 sets of data for each SPU, 5 Chunks per SPU of 96 Y pixels x 128 X pixels 
	uint8_t chunkData[5][5][96][128] __attribute__((aligned(8)));
	int summedAreaTable[5][24][8] __attribute__((aligned(16)));
	
	// 5 data addresses for each SPE
	VerticalChunkAddressStructure dataAddresses[5] __attribute__((aligned(16)));
	
	// calculate number of SPUs available to create contexts for
	int iSPUsAvailable = spe_cpu_info_get(SPE_COUNT_USABLE_SPES, -1);
	int iThreads = 5;
	
	// create a vector of contexts
	std::vector<spe_context_ptr_t> vecSPEContexts;
	std::vector<pthread_t> vecThreads;

	vecSPEContexts.resize(iThreads);
	vecThreads.resize(iThreads);
	ThreadArguementList list __attribute__((aligned(16)));
	int i;
	// create our SPU programs and run them using POSIX threads
	for(i = 0; i < vecThreads.size(); ++i)
	{
		if((vecSPEContexts[i] = spe_context_create(0, NULL)) == NULL)
		{
			perror("Failed creating SPE context");
			exit(1);
		}

		// load the program handle into the context
		if(spe_program_load(vecSPEContexts[i], &speHandle))
		{
			perror("Failed loading SPE program");
			exit(1);
		}
		
		// get our chunks for this SPU, 0 * 128 = 0, 1 * 128 = 128 etc.
		
		// get our chunks for this SPU, 0 * 128 = 0, 1 * 128 = 128 etc.
		verticallyChunkData(imageData, chunkData[i], dataAddresses[i], i * 128);
		dataAddresses[i].chunkData[5] = 0 + (unsigned)summedAreaTable[i];
		list.speCTX = vecSPEContexts[i];
		list.memoryAddressOfAddressData = &dataAddresses[i];
		
		if(pthread_create(&vecThreads[i], NULL, &ppu_pthread_function, &list))
		{	
			perror("Failed executing SPE Program");
			exit(1);
		}
		
	}
	
	// Check all our threads have completed and that it's safe to continue on
	for(i = 0; i < vecThreads.size(); ++i)
	{
		if(pthread_join(vecThreads[i], NULL))
		{
			perror("Failed pthread_join");
		}
		
		// Destroy Context 
		if(spe_context_destroy(vecSPEContexts[i]) != 0)
		{
			perror("Failed Destroying Context");
			exit(1);
		}
	}

	for(i = 0; i < 24; ++i)
	{
		memmove(&summedImage[i][0], &summedAreaTable[0][i], sizeof(summedAreaTable[0][i]));
		memmove(&summedImage[i][8], &summedAreaTable[1][i], sizeof(summedAreaTable[1][i]));
		memmove(&summedImage[i][16], &summedAreaTable[2][i], sizeof(summedAreaTable[2][i]));
		memmove(&summedImage[i][24], &summedAreaTable[3][i], sizeof(summedAreaTable[3][i]));
		memmove(&summedImage[i][32], &summedAreaTable[4][i], sizeof(summedAreaTable[4][i]));
	}
}

void saveCSV(std::vector < std::vector < uint8_t > > vectorData, char* fileName)
{
	std::ofstream MyFile;

	std::string ss;
	
	ss = fileName;
	ss += ".csv";
	

	MyFile.open((char*)ss.c_str(), std::ios::out);

	int counter = 0;

	for (int i = 0; i < 480; i++)
	{
		for (int j = 0; j < 640; j++)
		{
			MyFile << vectorData[i][j] << ",";
			
				
			counter++;

		}
		MyFile << '\n';
	}
	

	MyFile.close();

}


int main()
{
	// Timer variables	
	unsigned long long startTime;
	unsigned long long functionStart;

	int thresholdsForROI[10];
	
	thresholdsForROI[0] = 4200; // f
	thresholdsForROI[1] = 4400; // f: 4400 is good
	thresholdsForROI[2] = 3400; // f
	thresholdsForROI[3] = 3400; // f
	thresholdsForROI[4] = 4200; // f
	thresholdsForROI[5] = 4200; // f
	thresholdsForROI[6] = 4200; // f
	thresholdsForROI[7] = 3400; // f: 3400 is good
	thresholdsForROI[8] = 3400; // f: 3400 is good
	thresholdsForROI[9] = 4200; // f
	
	
	// all data when loaded into the test vector is accessed as [Y] for the first vector and [X] as the second
	// instead of the more intuitive [X][Y].
	std::vector< std::vector<uint8_t> > loadVector;
	std::vector< std::vector< std::vector<uint8_t> > > testVector __attribute__((aligned(16)));
	
	// load frame 1
	loadVector = loadCSV("Frame1.csv");
	testVector.push_back(loadVector);
	// load frame 2
	loadVector = loadCSV("Frame2.csv");
	testVector.push_back(loadVector);
	// load frame 3
	loadVector = loadCSV("Frame3.csv");
	testVector.push_back(loadVector);
	// load frame 4
	loadVector = loadCSV("Frame4.csv");
	testVector.push_back(loadVector);
	// load frame 5
	loadVector = loadCSV("Frame5.csv");
	testVector.push_back(loadVector);
	// load frame 6
	loadVector = loadCSV("Frame6.csv");
	testVector.push_back(loadVector);
	// load frame 7
	loadVector = loadCSV("Frame7.csv");
	testVector.push_back(loadVector);
	// load frame 8
	loadVector = loadCSV("Frame8.csv");
	testVector.push_back(loadVector);
	// load frame 9
	loadVector = loadCSV("Frame9.csv");
	testVector.push_back(loadVector);
	// load frame 10
	loadVector = loadCSV("Frame10.csv");
	testVector.push_back(loadVector);
	
	
	std::vector< std::vector< std::vector<uint8_t> > >  originalImage = testVector;
	std::vector< std::vector<uint8_t> > sobelImage;
	std::vector< std::vector<uint8_t> > thresholdedSobelImage;
	std::vector< std::vector<uint8_t> > roiImage;
	std::vector< std::vector<int> > iImage;
	iImage = std::vector< std::vector< int > >(24, std::vector< int >(40));

	std::vector< std::vector< std::vector<uint8_t> > > uneditedVector __attribute__((aligned(16)));
	uneditedVector = testVector;
	
	unsigned int numberOfTestLoops = 100;
	double times = 0;
/*
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			runSummedAreaTableSPE(sumfunction_spu, testVector[j], iImage);
				
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	testVector = uneditedVector;
	
	times = times / (10 * numberOfTestLoops);
	printf("sum function times %f\n", times);
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			runSummedAreaTableSPE(sobelandotsuandsum_spu, testVector[j], iImage);
				
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("sum + sobel + otsu spu times %f\n", times);
	times = 0;
	
		
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			testVector[9] = loadCSV("Frame10.csv");
			
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("loadCSV times %f\n", times);
	times = 0;
	
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
		
			runVerticalChunkSPE(sobel_spu, testVector[j]);

			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}

	times = times / (10 * numberOfTestLoops);
	printf("Sobel SPU Time %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
		
			runVerticalChunkSPE(otsu_spu, testVector[j]);

			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("Otsu SPU Time %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
		
			runVerticalChunkSPE(sobelandotsu_spu, testVector[j]);

			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("Sobel and Otsu SPU Time %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
		
			iImage = basicRegionSumming(testVector[j], 16, 20);
			
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("Basic Region Summing Time %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			applyROIToImage(originalImage[j], iImage, 16, 20, thresholdsForROI[j]);
			
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("Apply ROI To Image %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			saveCSV(testVector[j], "test");
			
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
			remove("test.csv");
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("saveCSV times %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			testVector[j] = sobelEdgeDetection(testVector[j], false);
			
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("Sobel Non-SPU times (unoptimized) %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			otsuThresholding(testVector[j]);
			
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("Otsu times Non-SPU %f\n", times);
	
	testVector = uneditedVector;
	times = 0;
	
	for(unsigned int i = 0; i < numberOfTestLoops; ++i)
	{
		for(unsigned int j = 0; j < 10; ++j)
		{
			functionStart = __mftb();
			
			integralImage(testVector[j], 16, 20);
			
			times += (double) ( ((double)__mftb() - (double)functionStart) / TICKS_PER_SEC );
		}
		
		testVector = uneditedVector;
	}
	
	times = times / (10 * numberOfTestLoops);
	printf("Integral Image Non-SPU %f\n", times);
	*/
	std::string fileName;

	std::stringstream out;

	for (unsigned int i = 0; i < testVector.size(); i++)
	{
		fileName = "initialImage";
		out << (i+1);
		fileName += out.str();
		saveCSV(testVector[i], (char*)fileName.c_str());
		out.str("");
	
		runVerticalChunkSPE(sobel_spu, testVector[i]);

		fileName = "sobelEdgeDetection";
		out << (i+1);
		fileName += out.str();
		saveCSV(testVector[i], (char*)fileName.c_str());
		out.str("");

		runVerticalChunkSPE(otsu_spu, testVector[i]);
		
		fileName = "afterOtsuThresholding";
		out << (i+1);
		fileName += out.str();
		saveCSV(testVector[i], (char*)fileName.c_str());
		out.str("");

		std::vector< std::vector<int> > iImage = basicRegionSumming(testVector[i], 16, 20);
		
		roiImage = applyROIToImage(originalImage[i], iImage, 16, 20, thresholdsForROI[i]);

		fileName = "ROIImage";
		out << (i+1);
		fileName += out.str();
		saveCSV(roiImage, (char*)fileName.c_str());
		out.str("");
	}
	
	printf("Done \n");
	
	return 0;
}
