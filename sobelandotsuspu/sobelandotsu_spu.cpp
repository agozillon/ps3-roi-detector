#include <stdio.h>
#include <stdint.h>
#include <spu_mfcio.h>
#include <string>
#include <algorithm>
#include <cmath>
#include <spu_intrinsics.h>

// first standard filter, flipped so that the first array is the x and second the y 
// when writing it the Y is top down so 0 is the top 2 is the bottom, 0, 0 would be the top leaf
// also [Y][X] when accessing array, (the loaded in data is also like this, so kept the array this way instead
// of re-arranging it.)
int sobelXKernel[3][3] = { { 1, 5, 1 },
{ 0, 0, 0 },
{ -1, -5, -1 } };


// same filter rotated 90 degrees, flipped so that the first array is the x and second the y 	
// when writing it the Y is top down so 0 is the top 2 is the bottom, 0, 0 would be the top left
// also [Y][X] when accessing array
int sobelYKernel[3][3] = { { -1, 0, 1 },
{ -5, 0, 5 },
{ -1, 0, 1 } };

// only really use 5, the sixth is for alignment (bus error otherwise)
// can be used for extra data 
struct VerticalChunkAddressStructure
{
	unsigned long long int chunkData[6];
};

/*
	Function that performs Otsu Thresholding on the Image Data it essentially makes a histogram of the
	data and then calculates where the threshold in relation to the total data should be. Normally
	we would do it with the whole of the image data but in this case we can do it with the chunked data
	with fairly little degredation in the outcome (even some improvements actually). Of note is that it 
	modifies the image data passed in and doesn't return it, so be aware of that.

	@uint8_t imageData - the image data in a 2d array that we wish to work on 
	@int xSize - number of elements the 2D array has on the X
	@int ySize - number of elements the 2D array has on the Y (Y is the first index in this case)
*/
void otsuThresholding(uint8_t imageData[][128], int xSize, int ySize)
{
	// Otsu method
	float threshold = 0;
	float varMax = 0;
	float sumBackground = 0;
	float sum = 0;
	int weightBackground = 0;
	int total;
	int weightForeground;
	float meanBackground;
	float meanForeground;
	float between;
	
	// Histogram of all the values we can attain from a greyscale image 1-256 (0-255 really)
	int histogram[256] = { 0 };

	int i, j;
	// create our histogram
	for (i = 0; i < ySize; ++i)
		for (j = 0; j < xSize; ++j)
			histogram[imageData[i][j]] ++;

	for (i = 1; i < 256; ++i)
		sum += i * histogram[i];

	// Calculate the Otsu Thresholding, essentially working out the median value between
	// the background and foreground so we can cut the background out
	for (i = 0; i < 256; ++i)
	{

		weightBackground += histogram[i];
		if (weightBackground == 0) continue;

		total = xSize * ySize;

		weightForeground = total - weightBackground;

		if (weightForeground == 0) break;

		sumBackground += (float)(i * histogram[i]);

		meanBackground = sumBackground / weightBackground;
		meanForeground = (sum - sumBackground) / weightForeground;
		between = (float)weightBackground * (float)weightForeground * pow(meanBackground - meanForeground, 2);

		if (between > varMax)
		{
			threshold = i;
			varMax = between;
		}

	}

	// Otsu thresholding check
	for (i = 0; i < ySize; ++i)
		for (j = 0; j < xSize; ++j)
		{
			if (imageData[i][j] < threshold)
				imageData[i][j] = 0;
		}
}

/*
	Function that performs sobel edge detection on the Image Data using the Sobel Kernels,
	of note is that it modifies the image data passed in and doesn't return it, so be aware of that.
	@uint8_t imageData - the image data in a 2d array that we wish to work on 
	@int xSize - number of elements the 2D array has on the X
	@int ySize - number of elements the 2D array has on the Y (Y is the first index in this case)
*/
void sobelEdgeDetection(uint8_t imageData[][128], int xSize, int ySize)
{
	int i, j, averageAtXPoint, averageAtYPoint, topLeft, centerLeft, bottomLeft, topCenter, bottomCenter, topRight, centerRight, bottomRight;
	int centerValue;
	
	uint8_t kernelValues[ySize][xSize];
	
	for (i = 0; __builtin_expect(i < ySize, 1); ++i)
	{
		for (j = 0; __builtin_expect(j < xSize, 1); ++j)
		{
			averageAtXPoint = 0; averageAtYPoint = 0;
			
			centerValue = imageData[i][j];
			
			topCenter = imageData[i - 1][j];
			centerLeft = imageData[i][j - 1];
			centerRight = imageData[i][j + 1];
			bottomCenter = imageData[i + 1][j];
			topLeft = imageData[i - 1][j - 1];
			topRight = imageData[i - 1][j + 1];
			bottomRight = imageData[i + 1][j + 1];
			bottomLeft = imageData[i + 1][j - 1];
			
			// based on what edge it goes off of, we copy the data from one of the existing neighbour cells
			// into it and use that instead

			// checking if the grid overextends
			// extends too far up
			if (__builtin_expect(i - 1 < 0, 0))
			{
				topCenter = centerValue;
				topLeft = centerValue;
				topRight = centerValue;
			}
			
			
			// extends too far down
			if (__builtin_expect(i + 1 >= ySize, 0))
			{
				bottomRight = centerValue;
				bottomLeft = centerValue;
				bottomCenter = centerValue;
			}
			
			// extends too far right
			if (__builtin_expect(j + 1 >= xSize, 0))
			{
				topRight = centerValue;
				centerRight = centerValue;
				bottomRight = centerValue;
			}
			
			// extends too far left
			if (__builtin_expect(j - 1 < 0, 0))
			{
				topLeft = centerValue;
				centerLeft = centerValue;
				bottomLeft = centerValue;
			}
			
			
			// Calculate X Points
			// top row, remember both Sobel and Data are accessed as [Y][X] not [X][Y]
			averageAtXPoint += topLeft; // left
			averageAtXPoint += topCenter * sobelXKernel[0][1];   // center
			averageAtXPoint += topRight; // right

			// bottom row
			averageAtXPoint += -bottomLeft; // left
			averageAtXPoint += bottomCenter * sobelXKernel[2][1]; // center
			averageAtXPoint += -bottomRight; // right

			// Calculate Y Points
			// top row, remember both Sobel and Data are accessed as [Y][X] not [X][Y]
			averageAtYPoint += -topLeft; // left
			averageAtYPoint += topRight; // right

			// middle row
			averageAtYPoint += centerLeft * sobelYKernel[1][0]; // center left * centered left value	
			averageAtYPoint += centerRight * sobelYKernel[1][2]; // center right * center right value

			// bottom row
			averageAtYPoint += -bottomLeft; // left
			averageAtYPoint += bottomRight; // right
			
			// exact magnitude as described in paper
			kernelValues[i][j] = (uint8_t)sqrt(pow((averageAtXPoint / 9), 2) + pow((averageAtYPoint / 9), 2));
		}
	}

	for (i = 0; i < ySize; ++i)
		memmove(imageData[i], kernelValues[i], sizeof(kernelValues[i])); 
			
}

/*
	Function that performs sobel edge detection on the Image Data using the Sobel Kernels,
	of note is that it modifies the image data passed in and doesn't return it, so be aware of that.
	@uint8_t imageData - the image data in a 2d array that we wish to work on 
	@int xSize - number of elements the 2D array has on the X
	@int ySize - number of elements the 2D array has on the Y (Y is the first index in this case)
*/
void sobelEdgeDetectionIntrins(uint8_t imageData[][128], int xSize, int ySize)
{
	int i, j;	
	uint8_t kernelValues[ySize][xSize];

	vec_float4 imageVals[8];
	vec_float4 kernelXValues[6];
	vec_float4 kernelYValues[6];
	vec_float4 averageAtXPoint = spu_splats(0.0f);
	vec_float4 averageAtYPoint = spu_splats(0.0f); 
	vec_float4 centerValues;
	
	vec_float4 reciprocalMultiple = spu_splats((float)1/9); 
	
	kernelXValues[0] = spu_splats(1.0f); // 0, 0 
	kernelXValues[1] = spu_splats(5.0f); // 0, 1
	kernelXValues[2] = spu_splats(1.0f); // 0, 2
	kernelXValues[3] = spu_splats(-1.0f); // 2, 0
	kernelXValues[4] = spu_splats(-5.0f); // 2, 1
	kernelXValues[5] = spu_splats(-1.0f); // 2, 2
	
	kernelYValues[0] = spu_splats(-1.0f); // 0, 0
	kernelYValues[1] = spu_splats(1.0f); // 0, 2
	kernelYValues[2] = spu_splats(-5.0f); // 1, 0
	kernelYValues[3] = spu_splats(5.0f); // 1, 2
	kernelYValues[4] = spu_splats(-1.0f); // 2, 0
	kernelYValues[5] = spu_splats(1.0f);  // 2, 2
	
	
	for (i = 0; __builtin_expect(i < ySize, 1); ++i)
	{
		for (j = 0; __builtin_expect(j < xSize, 1); j+=4)
		{
			centerValues = spu_insert((float)imageData[i][j], centerValues, 0);
			centerValues = spu_insert((float)imageData[i][j + 1], centerValues, 1);
			centerValues = spu_insert((float)imageData[i][j + 2], centerValues, 2);
			centerValues = spu_insert((float)imageData[i][j + 3], centerValues, 3);
			
			// top center, imageData[i - 1][j]
			imageVals[0] = spu_insert((float)imageData[i - 1][j], imageVals[0], 0);
			imageVals[0] = spu_insert((float)imageData[i - 1][(j) + 1], imageVals[0], 1);
			imageVals[0] = spu_insert((float)imageData[i - 1][(j) + 2], imageVals[0], 2);
			imageVals[0] = spu_insert((float)imageData[i - 1][(j) + 3], imageVals[0], 3);
			
			// center left, imageData[i][j - 1];
			imageVals[1] = spu_insert((float)imageData[i][j - 1], imageVals[1], 0);
			imageVals[1] = spu_insert((float)imageData[i][(j - 1) + 1], imageVals[1], 1);
			imageVals[1] = spu_insert((float)imageData[i][(j - 1) + 2], imageVals[1], 2);
			imageVals[1] = spu_insert((float)imageData[i][(j - 1) + 3], imageVals[1], 3);
			
			// center right, imageData[i][j + 1];
			imageVals[2] = spu_insert((float)imageData[i][j + 1], imageVals[2], 0);
			imageVals[2] = spu_insert((float)imageData[i][(j + 1) + 1], imageVals[2], 1);
			imageVals[2] = spu_insert((float)imageData[i][(j + 1) + 2], imageVals[2], 2);
			imageVals[2] = spu_insert((float)imageData[i][(j + 1) + 3], imageVals[2], 3);
			
			// bottom center, imageData[i + 1][j];
			imageVals[3] = spu_insert((float)imageData[i + 1][j], imageVals[3], 0);
			imageVals[3] = spu_insert((float)imageData[i + 1][j + 1], imageVals[3], 1);
			imageVals[3] = spu_insert((float)imageData[i + 1][j + 2], imageVals[3], 2);
			imageVals[3] = spu_insert((float)imageData[i + 1][j + 3], imageVals[3], 3);
		
			// top left, imageData[i - 1][j - 1];
			imageVals[4] = spu_insert((float)imageData[i - 1][j - 1], imageVals[4], 0);
			imageVals[4] = spu_insert((float)imageData[i - 1][(j - 1) + 1], imageVals[4], 1);
			imageVals[4] = spu_insert((float)imageData[i - 1][(j - 1) + 2], imageVals[4], 2);
			imageVals[4] = spu_insert((float)imageData[i - 1][(j - 1) + 3], imageVals[4], 3);
			
			// top right, imageData[i - 1][j + 1];
			imageVals[5] = spu_insert((float)imageData[i - 1][j + 1], imageVals[5], 0);
			imageVals[5] = spu_insert((float)imageData[i - 1][(j + 1) + 1], imageVals[5], 1);
			imageVals[5] = spu_insert((float)imageData[i - 1][(j + 1) + 2], imageVals[5], 2);
			imageVals[5] = spu_insert((float)imageData[i - 1][(j + 1) + 3], imageVals[5], 3);
			
			// bottom right, imageData[i + 1][j + 1];
			imageVals[6] = spu_insert((float)imageData[i + 1][j + 1], imageVals[6], 0);
			imageVals[6] = spu_insert((float)imageData[i + 1][(j + 1) + 1], imageVals[6], 1);
			imageVals[6] = spu_insert((float)imageData[i + 1][(j + 1) + 2], imageVals[6], 2);
			imageVals[6] = spu_insert((float)imageData[i + 1][(j + 1) + 3], imageVals[6], 3);
			
			// bottom left, imageData[i + 1][j - 1];
			imageVals[7] = spu_insert((float)imageData[i + 1][j - 1], imageVals[7], 0);
			imageVals[7] = spu_insert((float)imageData[i + 1][(j - 1) + 1], imageVals[7], 1);
			imageVals[7] = spu_insert((float)imageData[i + 1][(j - 1) + 2], imageVals[7], 2);
			imageVals[7] = spu_insert((float)imageData[i + 1][(j - 1) + 3], imageVals[7], 3);
			
			// based on what edge it goes off of, we copy the data from one of the existing neighbour cells
			// into it and use that instead 

			// checking if the grid overextends
			// extends too far up
			if (__builtin_expect(i - 1 < 0, 0))
			{
				imageVals[0] = centerValues;
				imageVals[4] = centerValues;
				imageVals[5] = centerValues;
			}
			
			
			// extends too far down
			if (__builtin_expect(i + 1 >= ySize, 0))
			{
				imageVals[7] = centerValues;
				imageVals[6] = centerValues; 
				imageVals[3] = centerValues;
			}
			
			// extends too far right
			if (__builtin_expect((j + 3) + 1 >= xSize, 0))
			{
				imageVals[5] = spu_insert(imageData[i][j + 3], imageVals[5], 3);
				imageVals[6] = spu_insert(imageData[i][j + 3], imageVals[6], 3); 
				imageVals[2] = spu_insert(imageData[i][j + 3], imageVals[2], 3);
			}
			// bottom left 7, top left 4, center left 1
			// extends too far left
			if (__builtin_expect(j - 1 < 0, 0))
			{
				imageVals[7] = spu_insert(imageData[i][j], imageVals[7], 0);
				imageVals[4] = spu_insert(imageData[i][j], imageVals[4], 0);
				imageVals[1] = spu_insert(imageData[i][j], imageVals[1], 0);
			}
			
			averageAtXPoint = spu_splats(0.0f);
			averageAtYPoint = spu_splats(0.0f);
			
			//	topLeft * sobelXKernel[0][0];
			averageAtXPoint = spu_madd(imageVals[4], kernelXValues[0], averageAtXPoint);
			
			// topCenter * sobelXKernel[0][1]; 
			averageAtXPoint = spu_madd(imageVals[0], kernelXValues[1], averageAtXPoint);
	
			// topRight * sobelXKernel[0][2]; 
			averageAtXPoint = spu_madd(imageVals[5], kernelXValues[2], averageAtXPoint);
	
			// bottomLeft * sobelXKernel[2][0]; 
			averageAtXPoint = spu_madd(imageVals[7], kernelXValues[3], averageAtXPoint);
			
			// bottomCenter * sobelXKernel[2][1]; 
			averageAtXPoint = spu_madd(imageVals[3], kernelXValues[4], averageAtXPoint);
			
			// bottomRight * sobelXKernel[2][2]; 
			averageAtXPoint = spu_madd(imageVals[6], kernelXValues[5], averageAtXPoint);
			
			// topLeft * sobelYKernel[0][0]; 
			averageAtYPoint = spu_madd(imageVals[4], kernelYValues[0], averageAtYPoint);
		
			// topRight * sobelYKernel[0][2]; 
			averageAtYPoint = spu_madd(imageVals[5], kernelYValues[1], averageAtYPoint);
			
			// centerLeft * sobelYKernel[1][0]; 
			averageAtYPoint = spu_madd(imageVals[1], kernelYValues[2], averageAtYPoint);
			
			// centerRight * sobelYKernel[1][2]; 
			averageAtYPoint = spu_madd(imageVals[2], kernelYValues[3], averageAtYPoint);
			
			// bottomLeft * sobelYKernel[2][0]; 
			averageAtYPoint = spu_madd(imageVals[7], kernelYValues[4], averageAtYPoint);
			
			// bottomRight * sobelYKernel[2][2];
			averageAtYPoint = spu_madd(imageVals[6], kernelYValues[5], averageAtYPoint);

			
			// average by using reciprocal / 9
			averageAtYPoint = spu_mul(averageAtYPoint, reciprocalMultiple);
			averageAtXPoint = spu_mul(averageAtXPoint, reciprocalMultiple);
		
			averageAtYPoint = spu_mul(averageAtYPoint, averageAtYPoint);
			averageAtXPoint = spu_mul(averageAtXPoint, averageAtXPoint);
			
			// power of 2 and then addition of each other
			averageAtXPoint = spu_add(averageAtXPoint, averageAtYPoint);
			
			averageAtYPoint = spu_rsqrte(averageAtXPoint);
			averageAtYPoint = spu_mul(averageAtXPoint, averageAtYPoint);

		
			// exact magnitude as described in paper
			kernelValues[i][j] = (uint8_t)spu_extract(averageAtYPoint, 0); 
			kernelValues[i][j + 1] = (uint8_t)spu_extract(averageAtYPoint, 1); 
			kernelValues[i][j + 2] = (uint8_t)spu_extract(averageAtYPoint, 2); 
			kernelValues[i][j + 3] = (uint8_t)spu_extract(averageAtYPoint, 3); 

			
		}
	}

	for (i = 0; i < ySize; ++i)
		memmove(imageData[i], kernelValues[i], sizeof(kernelValues[i])); 
			
}

// function that pulls the chunk data from the DMA bus
// @uint8_t** appendData - pointer to an 1D array that we append the chunk to
// @int startRow - the integer place we start to append the data from, continues on for 96 more rows
// 				   as the chunk data should be received in 96 * 128 packets (well the memory addresses)
// 				   and altogether the appendData will eventually reach [480][128] one downward slice of the image
//				   out of 5
// @int chunkNumber - index value of the chunk we wish to access within the address structure
// @VerticalChunkAddressStructure dataAddresses - structure containing the sobel memory addresses, should already
// 										    be filled using mfc_get
void getChunkData(uint8_t appendData[][128], int startRow, int chunkNumber, VerticalChunkAddressStructure dataAddresses)
{
	uint8_t chunks[96][128];
	// works to bring in 2D array data
	mfc_get(&chunks, dataAddresses.chunkData[chunkNumber], sizeof(chunks), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
	
	int chunkI = 0;
	for(int i = startRow; i < startRow + 96; ++i, ++chunkI)
		memmove(&appendData[i], &chunks[chunkI], sizeof(chunks[chunkI]));
}

// function that puts the chunk data back onto the DMA bus/shared memory
// @uint8_t** appendData - pointer to an 1D array that we append the chunk to
// @int startRow - the integer place we start to append the data from, continues on for 96 more rows
// 				   as the chunk data should be received in 96 * 128 packets (well the memory addresses)
// 				   and altogether the appendData will eventually reach [480][128] one downward slice of the image
//				   out of 5
// @int chunkNumber - index value of the chunk we wish to access within the address structure
// @VerticalChunkAddressStructure dataAddresses - structure containing the sobel memory addresses, should already
// 										    be filled using mfc_get
void putChunkData(uint8_t appendData[][128], int startRow, int chunkNumber, VerticalChunkAddressStructure dataAddresses)
{
	uint8_t chunks[96][128];
	
	int chunkI = 0;
	for(int i = startRow; i < startRow + 96; ++i, ++chunkI)
		memmove(&chunks[chunkI], &appendData[i], sizeof(appendData[i]));
		
	mfc_put(chunks, dataAddresses.chunkData[chunkNumber], sizeof(chunks), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
}

int main(unsigned long long spe_id, unsigned long long argp,
 unsigned long long envp)
{
	// 60kb's of data so we can split the image into 6x
	VerticalChunkAddressStructure dataAddresses __attribute__((aligned(16)));
	
	// should be uint8_t && 96 * 128 (maybe more would need to recalculate)
	// get the memory addresses of the data
	mfc_get(&dataAddresses, argp, sizeof(dataAddresses), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
	
	uint8_t combinedData[480][128];
	
	getChunkData(combinedData, 0, 0, dataAddresses);
	getChunkData(combinedData, 96, 1, dataAddresses);
	getChunkData(combinedData, 192, 2, dataAddresses);
	getChunkData(combinedData, 288, 3, dataAddresses);
	getChunkData(combinedData, 384, 4, dataAddresses);
		
	sobelEdgeDetectionIntrins(combinedData, 128, 480);
	otsuThresholding(combinedData, 128, 480);
	
	putChunkData(combinedData, 0, 0, dataAddresses);
	putChunkData(combinedData, 96, 1, dataAddresses);
	putChunkData(combinedData, 192, 2, dataAddresses);
	putChunkData(combinedData, 288, 3, dataAddresses);
	putChunkData(combinedData, 384, 4, dataAddresses);

	return 0;
}