#include <stdio.h>
#include <stdint.h>
#include <spu_mfcio.h>
#include <string>
#include <algorithm>
#include <cmath>

// only really use 5, the sixth is for alignment (bus error otherwise)
// can be used for extra data 
struct VerticalChunkAddressStructure
{
	// Sixth element in the array is used for the summed area table
	unsigned long long int chunkData[6];
};

// function that pulls the chunk data from the DMA bus
// @uint8_t** appendData - pointer to an 1D array that we append the chunk to
// @int startRow - the integer place we start to append the data from, continues on for 96 more rows
// 				   as the chunk data should be received in 96 * 128 packets (well the memory addresses)
// 				   and altogether the appendData will eventually reach [480][128] one downward slice of the image
//				   out of 5
// @int chunkNumber - index value of the chunk we wish to access within the address structure
// @VerticalChunkAddressStructure dataAddresses - structure containing the sobel memory addresses, should already
// 										    be filled using mfc_get
void getChunkData(uint8_t appendData[][128], int startRow, int chunkNumber, VerticalChunkAddressStructure dataAddresses)
{
	uint8_t chunks[96][128];
	// works to bring in 2D array data 
	mfc_get(&chunks, dataAddresses.chunkData[chunkNumber], sizeof(chunks), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
	
	int chunkI = 0;
	for(int i = startRow; i < startRow + 96; ++i, ++chunkI)
		memmove(&appendData[i], &chunks[chunkI], sizeof(chunks[chunkI]));
}

// function that puts the chunk data back onto the DMA bus/shared memory
// @uint8_t** appendData - pointer to an 1D array that we append the chunk to
// @int startRow - the integer place we start to append the data from, continues on for 96 more rows
// 				   as the chunk data should be received in 96 * 128 packets (well the memory addresses)
// 				   and altogether the appendData will eventually reach [480][128] one downward slice of the image
//				   out of 5
// @int chunkNumber - index value of the chunk we wish to access within the address structure
// @VerticalChunkAddressStructure dataAddresses - structure containing the sobel memory addresses, should already
// 										    be filled using mfc_get
void putChunkData(uint8_t appendData[][128], int startRow, int chunkNumber, VerticalChunkAddressStructure dataAddresses)
{
	uint8_t chunks[96][128];
	
	int chunkI = 0;
	for(int i = startRow; i < startRow + 96; ++i, ++chunkI)
		memmove(&chunks[chunkI], &appendData[i], sizeof(appendData[i]));
		
	mfc_put(chunks, dataAddresses.chunkData[chunkNumber], sizeof(chunks), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
}

void basicRegionSumming(uint8_t imageData[][128], unsigned int sizeOfRegionInX, unsigned int sizeOfRegionInY, int tableData[24][8])
{
// The Table all our results will end up in and then be returned
	int sumAreaTable[24][8];
		
	// the table we sum the results into to get each integral value for each region
	int sumAreaRegion[20][16];
		
	for (unsigned int j2 = 0; j2 < 24; j2++)
	{
		for (unsigned int i2 = 0; i2 < 8; i2++)
		{
			unsigned int regionXStart = i2 * sizeOfRegionInX;
			unsigned int regionYStart = j2 * sizeOfRegionInY;
			for (unsigned int i3 = 0; i3 < sizeOfRegionInY; i3++)
			{
				for (unsigned int j3 = 0; j3 < sizeOfRegionInX; j3++)
				{
					sumAreaRegion[i3][j3] = imageData[regionYStart + i3][regionXStart + j3];
				}
			}
			// loop through all x and y, current value += previous X Value + previous Y Value -  
			for (unsigned int i = 0; i < sizeOfRegionInY; i++)
			{
				for (unsigned int j = 0; j < sizeOfRegionInX; j++)
				{
					sumAreaTable[j2][i2] += sumAreaRegion[i][j];
				}
			}
			
			tableData[j2][i2] = sumAreaTable[j2][i2];
		}
	}
}

int main(unsigned long long spe_id, unsigned long long argp,
 unsigned long long envp)
{
	// 60kb's of data so we can split the image into 6x
	VerticalChunkAddressStructure dataAddresses __attribute__((aligned(16)));
	
	// should be uint8_t && 96 * 128 (maybe more would need to recalculate)
	// get the memory addresses of the datas
	mfc_get(&dataAddresses, argp, sizeof(dataAddresses), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();
	
	uint8_t combinedData[480][128];
	
	getChunkData(combinedData, 0, 0, dataAddresses);
	getChunkData(combinedData, 96, 1, dataAddresses);
	getChunkData(combinedData, 192, 2, dataAddresses);
	getChunkData(combinedData, 288, 3, dataAddresses);
	getChunkData(combinedData, 384, 4, dataAddresses);

	
	int tableData[24][8] __attribute__((aligned(16)));
	
	basicRegionSumming(combinedData, 16, 20, tableData);
	
	mfc_put(tableData, dataAddresses.chunkData[5], sizeof(tableData), 0, 0, 0);
	mfc_write_tag_mask(1<<0);
	mfc_read_tag_status_any();

	return 0;
}
